# vtt-settings

Sample FoundryVTT module demonstrating functionality of settings.

```javascript
// Register a simple tickbox setting
game.settings.register("moduleName", "settingId", {
    name: "Setting's Title",
    hint: "Description of the setting",
    scope: "world", // set "client" to store setting only in the current client app (e.g. your browser)
    config: true, // set false to hide from user
    type: Boolean, // Boolean, Number, String, or any custom Object
    default: false,
    onChange: value => {
      console.log(`Tutorial | Local Hosting Solution changed to: ${JSON.stringify(value)}`)
    }
})

// Get current value of a setting
game.settings.get("moduleName", "settingId")

// Set new value of a setting
game.settings.set("moduleName", "settingId", True)
```
const moduleName = "vtt-settings"; // Must be same as "name" value in module.json

Hooks.once("init", () => {

  // Boolean global setting
  game.settings.register(moduleName, "Hosting", {
    name: "Local Hosting Solution",
    hint: "Is Foundry being hosted on your computer?",
    scope: "world",
    config: true,
    type: Boolean,
    default: false,
    onChange: value => {
      console.log(`Tutorial | Local Hosting Solution changed to: ${JSON.stringify(value)}`)
    }
  })

  // Text local setting
  game.settings.register(moduleName, "System", {
    name: "Favourite System",
    hint: "What is your favourite system?",
    scope: "client",
    config: true,
    type: String,
    choices: {
      "DnD": "Dungeons & Dragons 5th Edition",
      "VtM": "Vampire: The Masquerade 5th Edition",
      "L5R": "Legend of the Five Rings 5th Edition"
    },
    default: "1",
    onChange: value => {
      console.log(`Tutorial | Favourite System changed to: ${JSON.stringify(value)}`)
      updateSecretSetting()
    }
  })

  // Range setting
  game.settings.register(moduleName, "Tenure", {
    name: "Gaming Tenure",
    hint: "How many years have you been playing RPGs?",
    scope: "client",
    config: true,
    type: Number,
    range: {
      min: 0,
      max: 100,
      step: 1
    },
    default: "3",
    onChange: value => {
      console.log(`Tutorial | Gaming Tenure changed to: ${JSON.stringify(value)}`)
      updateSecretSetting()
    }
  })

  // Hidden setting with Object
  game.settings.register(moduleName, "Secret", {
    name: "Gaming Profile",
    hint: "Have you been gaming for over a decade?",
    scope: "client",
    config: false,
    type: GamerProfile,
    onChange: value => {
      console.log(`Tutorial | Secret profile changed to: ${JSON.stringify(value)}`)
    }
  })

  // Popout submenu
   game.settings.registerMenu(moduleName, "mySettingsMenu", {
     name: "Submenu",
     label: "Button label",
     hint: "This will open a simple HTML template",
     icon: "fas fa-mask",
     type: CustomSubMenu,
     restricted: false
   });

})

const updateSecretSetting = () => {
  let system = game.settings.get(moduleName, "System")
  let tenure = game.settings.get(moduleName, "Tenure")
    
}

class GamerProfile {
  constructor(system, years) {
    this.favoriteSystem = system
    this.yearsPlaying = years
  }
}

// Minimum viable submenu class
class CustomSubMenu extends FormApplication {

  static get defaultOptions() {
    const options = super.defaultOptions;
    options.template = "modules/vtt-settings/templates/submenu.html";
    return options;
  }

  async getData(options) {
    return new Object()
  }
}